#!/bin/sh

# Test created by Paul Wise (pabs) for hexedit on Debian.

set -e

rm -f nonempty empty result

echo -n nonempty > nonempty
echo -n > empty
echo -n result > result
for f in nonempty empty ; do
  echo "Editing $f in hexedit"
  printf "$f"'\n\tresult\33ty\30y' |
  TERM=xterm pipetty hexedit 2>&1 |
  ansi2txt |
  tr '\r' '\n'
done
head -vn-0 nonempty empty result ; echo
for f in nonempty empty ; do
  cmp "$f" result
done
